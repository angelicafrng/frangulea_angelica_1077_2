
const FIRST_NAME = "Frangulea";
const LAST_NAME = "Angelica Cristina";
const GRUPA = "1077C";

/**
 * Make the implementation here
 */
function initCaching() {
   var cache={'home':0,'about':0,'contact':0};
   cache.pageAccessCounter=function(value){
    if(value==undefined||value.toLowerCase()=='home')
        this.home++;
    else if(value.toLowerCase()=='about')
        this.about++;
    else if(value.toLowerCase()=='contact')
        this.contact++;

   }

   cache.getCache=function(){
        return this;
   }

   return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

